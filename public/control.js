let midiData = [];
let webSocket;
let firstNoteOn = false;

window.onload = () => {
    document.getElementById('init').onclick = () => {
        MIDI.loadPlugin({
            soundfontUrl: 'soundfont/',
            onprogress: onProgress
        });
    };

    document.getElementById('load').onclick = () => {
        let fileReader = new FileReader();
        fileReader.onloadend = () => {
            MIDI.Player.loadFile('base64,' + btoa(fileReader.result), undefined, onProgress);
        };
        fileReader.readAsBinaryString(document.getElementById('midi-file').files[0]);
    };

    document.getElementById('play').onclick = () => {
        MIDI.Player.removeListener();
        firstNoteOn = false;
        MIDI.Player.addListener((data) => {
            if(data.message !== 144 && data.message !== 128) return;
            let state = data.message === 144;
            if(!firstNoteOn && !state) return;
            firstNoteOn = true;
            midiData.push({
                channel: data.channel,
                note: data.note,
                now: Math.round(data.now * 10) / 10,
                velocity: data.velocity,
                state: state
            });
        });
        MIDI.Player.start();
    };

    document.getElementById('stop').onclick = () => {
        MIDI.Player.stop();
    };

    document.getElementById('conn').onclick = () => {
        webSocket = new WebSocket('ws://' + window.location.host + '/control');
    };

    document.getElementById('send').onclick = () => {
        webSocket.send('midiData ' + JSON.stringify(midiData));
    };

    document.getElementById('fire').onclick = () => {
        webSocket.send('fire');
    };
};

function onProgress(state, progress) {
    console.log(state, progress);
}