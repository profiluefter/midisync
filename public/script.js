window.onload = () => {
    writeStatus('Loaded Javascript! Loading MIDI:');
    MIDI.loadPlugin({
        soundfontUrl: 'soundfont/',
        onprogress: (state, progress) => writeStatus(state + (progress * 100) + '%'),
        onsuccess: () => {
            writeStatus('Loaded MIDI! Waiting for connect: ');
            setButton(true, 'Connect',
                () => {
                    writeStatus('Connecting...');
                    let serverSocket = new WebSocket('ws://' + window.location.host + '/client');
                    serverSocket.onopen = () => {
                        writeStatus('Connected! Loading infos...');
                        serverSocket.onmessage = ev => {
                            serverSocket.onmessage = undefined;
                            if(ev.data === 'registered') {
                                writeStatus('Registered! Ready to play!');
                                setButton(false, undefined, undefined);

                                serverSocket.onmessage = event => {
                                    let data = JSON.parse(event.data);
                                    console.log(JSON.stringify(data));
                                    MIDI.noteOn(0, data.note, data.velocity, (data.timestamp - Date.now()) / 1000);
                                    MIDI.noteOff(0, data.note, data.velocity, (data.timestamp - Date.now() + data.duration) / 1000);
                                };
                            } else {
                                alert('Unknown server response: ' + ev.data);
                            }
                        };
                        serverSocket.send('register');
                    };
                }
            );
        }
    });
};

function writeStatus(status) {
    document.getElementById('status').innerText = status;
}

function setButton(visible, text, callback) {
    let button = document.getElementById('button');
    button.style.display = visible ? 'unset' : 'none';
    button.innerText = text;
    button.onclick = callback;
}