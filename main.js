const delaySeconds = 2;

let deviceList = [];
let midiNotes = [];

function main() {
    function expressServer() {
        const express = require('express');
        const app = express();
        require('express-ws')(app);

        app.use(express.static('public'));

        app.ws('/client', (ws, req) => {
            console.log('Client-device connected: ' + req.ip);
            ws.on('message', (msg) => {
                switch(msg.split(' ')[0]) {
                    case 'register':
                        registerDevice(ws, msg);
                        break;
                    default:
                        console.warn('Unknown message: ' + msg);
                        break;
                }
            });
        });

        app.ws('/control', (ws, req) => {
            console.log('Control-device connected: ' + req.ip);
            ws.on('message', (msg) => {
                switch(msg.split(' ')[0]) {
                    case 'fire':
                        sendCommands(ws, msg);
                        break;
                    case 'midiData':
                        midiNotes = JSON.parse(msg.split(' ')[1]);
                        break;
                    default:
                        console.warn('Unknown message: ' + msg);
                        break;
                }
            });
        });

        app.listen(8080);
    }

    expressServer();
}

function registerDevice(ws, msg) {
    //TODO: Handle onClose event
    deviceList.push(ws);
    ws.send('registered');
}

function sendCommands() {
    let deviceCount = deviceList.length;
    let deviceCounter = 0;

    function sendNote(timestamp, note, velocity, duration) {
        timestamp += delaySeconds * 1000;

        console.log(deviceCounter);
        let device = deviceList[deviceCounter++];
        device.send(JSON.stringify({timestamp, note, velocity, duration}));
        deviceCounter %= deviceCount;
    }

    let processedMidi = [];
    let notes = new Map();
    for(let rawMidi of midiNotes) {
        console.log(JSON.stringify(rawMidi));
        if(rawMidi.state)
            notes.set(rawMidi.channel * 10000 + rawMidi.note, [rawMidi.now, rawMidi.velocity]);
        else {
            let startTimestamp = notes.get(rawMidi.channel * 10000 + rawMidi.note);
            if(typeof startTimestamp === 'undefined') continue;
            processedMidi.push({
                now: startTimestamp[0],
                note: rawMidi.note,
                velocity: startTimestamp[1],
                duration: rawMidi.now - startTimestamp[0]
            });
        }
    }

    let timestampNow = Date.now();
    for(let midiNote of processedMidi) {
        sendNote(timestampNow + midiNote.now, midiNote.note, midiNote.velocity, midiNote.duration);
    }
}

main();
